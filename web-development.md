# Web Development

- [HTML](html.md)
- [CSS](css.md)
- [JavaScript](javascript.md)
- [Frontend frameworks](frontend-frameworks.md)
- [Static Websites](static-websites.md)
- [Dynamic Websites](dynamic-websites.md)
