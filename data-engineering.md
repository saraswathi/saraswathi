# Data Engineering

## Websites

## Books

- [Fundamentals of Data Engineering: Plan and Build Robust Data Systems by Joe Reis, Matt Housley](https://amzn.to/3OWmd0q)
- [Data Engineering with Python: Work with massive datasets to design data models and automate data pipelines using Python by Paul Crickard](https://amzn.to/3EpCjLk)
- [ Data Engineering with AWS: Learn how to design and build cloud-based data transformation pipelines using AWS](https://amzn.to/3sEEtnC)

## Videos

- [Data Engineering Course | Become A Data Engineer | Intellipaat](https://www.youtube.com/watch?v=OoHPhLV43gg)

## Courses

## User groups

## Syllabus

- [GNU/Linux](gnu-linux.md)
- [SQL](sql.md)
- [Python](python.md)
- [PySpark](pyspark.md)
- [HDFS](hdfs.md)
- [Hive](hive.md)
- [Sqoop](sqoop.md)
- [Kafka](kafka.md)
- [Cassandra](cassandra.md)
- [MongoDB](mongodb.md)
- [Superset](superset.md)
- [Airflow](airflow.md)
- [Cloud Computing](cloud-computing.md)
